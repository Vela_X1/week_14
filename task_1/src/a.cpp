#include <iostream>
#include <vector>

template<class T>
class Stack {
    private:
        std::vector<T> elements;

    public:
        bool empty() const {
        return elements.empty();
}

        size_t size() const {
            return elements.size();
}

void push(const T& value) {
    elements.push_back(value);
}

T pop() {
if (empty()) {
    throw std::out_of_range("Stack is empty");
}
    T value = elements.back();
    elements.pop_back();
        return value;
}

T& top() {
if (empty()) {
    throw std::out_of_range("Stack is empty");
}
        return elements.back();
}
};

int main() {
    Stack<int> stack;
    stack.push(1);
    stack.push(2);
    stack.push(3);

        std::cout << "Stack size: " << stack.size() << std::endl;
        std::cout << "Top element: " << stack.top() << std::endl;

        int poppedElement = stack.pop();
            std::cout << "Popped element: " << poppedElement << std::endl;

                std::cout << "Stack size after pop: " << stack.size() << std::endl;

                    return 0;
}
